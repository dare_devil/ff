import React from 'react';

const Index = ({ children }) => {
  return <div className="container-fluid">{children}</div>;
};

export default Index;
