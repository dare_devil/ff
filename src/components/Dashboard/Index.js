import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { getRandomWord } from '../../helpers/Index';
import Timer from '../Timer/Index';

const Dashboard = () => {
  const initialState = {
    randomWordInput: '',
  };
  const { playerName } = useParams();
  const [time, setTime] = useState({ ms: 99, s: 2 });
  const [randomWord, setRandowmWord] = useState(getRandomWord());
  const [values, setValues] = useState(initialState);
  let player = {};
  let updatedS = time.s;
  let updatedMs = time.ms;

  const calculateTimerValue = () => {
    const randomWordLength = randomWord.length;
    const difficultyFactor = 1;

    const timerValue = randomWordLength / difficultyFactor;
    if (timerValue < 2) {
      timerValue = 2;
    }
    setTime({ ms: 99, s: timerValue });
  };

  const handelChange = (e) => {
    let { name, value } = e.target;
    setValues((values) => ({
      ...values,
      [name]: value,
    }));
  };

  const handleKeyPress = (e) => {
    let { name, value } = e.target;
    if (value === randomWord) {
      setValues({ randomWordInput: '' });
      setRandowmWord(getRandomWord());
      calculateTimerValue();
    }
  };

  const countDown = () => {
    if (updatedMs === 0) {
      updatedS -= 1;
      updatedMs = 99;
    }

    updatedMs -= 1;

    return setTime({ ms: updatedMs, s: updatedS });
  };

  useEffect(() => {
    if (time.s > 0 || time.ms > 0) {
      setTimeout(() => countDown(), 10);
    }
  });

  // Check if the player exists

  if (!(localStorage.getItem(playerName) === null)) {
    player = JSON.parse(localStorage.getItem(playerName));
  } else {
    // Redirect to home
    window.location = '/';
  }
  return (
    <div className="d-flex align-items-center justify-content-center vh-100">
      <Timer time={time}></Timer>
      <div className="randomWord-container">
        <div className="randomWord-text">{randomWord}</div>
        <form>
          <div className="form-group">
            <input
              type="text"
              placeholder="type your name"
              name="randomWordInput"
              className="form-control player-input"
              onChange={(e) => handelChange(e)}
              onKeyUp={(e) => handleKeyPress(e)}
              value={values.randomWordInput}
            />
          </div>
        </form>
      </div>
    </div>
  );
};

export default Dashboard;
